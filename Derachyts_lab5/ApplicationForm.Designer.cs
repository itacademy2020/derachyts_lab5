﻿
namespace Derachyts_lab5
{
    partial class ApplicationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbAccountantConf = new System.Windows.Forms.ComboBox();
            this.cmbManagerConf = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbWorker = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cityBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.countryBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateEndBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateStartBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.reasonBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvAppRes = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAppRes)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button3);
            this.splitContainer1.Panel1.Controls.Add(this.button2);
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvAppRes);
            this.splitContainer1.Size = new System.Drawing.Size(1177, 514);
            this.splitContainer1.SplitterDistance = 227;
            this.splitContainer1.TabIndex = 0;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(22, 479);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(184, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Save changes";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(21, 448);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Show Results";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 419);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Add new application";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbAccountantConf);
            this.groupBox1.Controls.Add(this.cmbManagerConf);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cmbWorker);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.cityBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.countryBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateEndBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dateStartBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.reasonBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 401);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter data";
            // 
            // cmbAccountantConf
            // 
            this.cmbAccountantConf.FormattingEnabled = true;
            this.cmbAccountantConf.Location = new System.Drawing.Point(9, 226);
            this.cmbAccountantConf.Name = "cmbAccountantConf";
            this.cmbAccountantConf.Size = new System.Drawing.Size(185, 21);
            this.cmbAccountantConf.TabIndex = 20;
            // 
            // cmbManagerConf
            // 
            this.cmbManagerConf.FormattingEnabled = true;
            this.cmbManagerConf.Location = new System.Drawing.Point(9, 181);
            this.cmbManagerConf.Name = "cmbManagerConf";
            this.cmbManagerConf.Size = new System.Drawing.Size(185, 21);
            this.cmbManagerConf.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Accountant confirmation";
            // 
            // cmbWorker
            // 
            this.cmbWorker.FormattingEnabled = true;
            this.cmbWorker.Location = new System.Drawing.Point(9, 369);
            this.cmbWorker.Name = "cmbWorker";
            this.cmbWorker.Size = new System.Drawing.Size(185, 21);
            this.cmbWorker.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 353);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Select Worker";
            // 
            // cityBox
            // 
            this.cityBox.Location = new System.Drawing.Point(9, 319);
            this.cityBox.Name = "cityBox";
            this.cityBox.Size = new System.Drawing.Size(185, 20);
            this.cityBox.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 303);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "City";
            // 
            // countryBox
            // 
            this.countryBox.Location = new System.Drawing.Point(9, 270);
            this.countryBox.Name = "countryBox";
            this.countryBox.Size = new System.Drawing.Size(185, 20);
            this.countryBox.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 254);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Country";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Manager\'s confirmation";
            // 
            // dateEndBox
            // 
            this.dateEndBox.Location = new System.Drawing.Point(10, 130);
            this.dateEndBox.Name = "dateEndBox";
            this.dateEndBox.Size = new System.Drawing.Size(184, 20);
            this.dateEndBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Date end";
            // 
            // dateStartBox
            // 
            this.dateStartBox.Location = new System.Drawing.Point(10, 81);
            this.dateStartBox.Name = "dateStartBox";
            this.dateStartBox.Size = new System.Drawing.Size(184, 20);
            this.dateStartBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date start";
            // 
            // reasonBox
            // 
            this.reasonBox.Location = new System.Drawing.Point(10, 37);
            this.reasonBox.Name = "reasonBox";
            this.reasonBox.Size = new System.Drawing.Size(184, 20);
            this.reasonBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Reason";
            // 
            // dgvAppRes
            // 
            this.dgvAppRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAppRes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAppRes.Location = new System.Drawing.Point(0, 0);
            this.dgvAppRes.Name = "dgvAppRes";
            this.dgvAppRes.Size = new System.Drawing.Size(946, 514);
            this.dgvAppRes.TabIndex = 0;
            this.dgvAppRes.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.cellChanged);
            this.dgvAppRes.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.tableDataValidation);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 514);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form4";
            this.Text = "Applications";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAppRes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox dateEndBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox dateStartBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox reasonBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbWorker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox cityBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox countryBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvAppRes;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbAccountantConf;
        private System.Windows.Forms.ComboBox cmbManagerConf;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}