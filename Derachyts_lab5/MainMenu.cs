﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class MainMenu : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public MainMenu()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CrewMemberForm Form2 = new CrewMemberForm();
            Form2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            WorkerForm Form3 = new WorkerForm();
            Form3.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ApplicationForm Form4 = new ApplicationForm();
            Form4.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            AdvancedReportForm Form5 = new AdvancedReportForm();
            Form5.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FirstReport Form6 = new FirstReport();
            Form6.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            SecondReport Form7 = new SecondReport();
            Form7.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ThirdReport Form8 = new ThirdReport();
            Form8.Show();
        }
    }
}
