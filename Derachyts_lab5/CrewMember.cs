﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Derachyts_lab5
{
    class CrewMember
    {
        public int id { get; set; }
        public string full_name { get; set; }
        public int age { get; set; }
        public string adress { get; set; }
        public int position_id { get; set; }

        public override string ToString()
        {
            return $"{full_name}";
        }
    }
}
