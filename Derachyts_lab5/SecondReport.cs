﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class SecondReport : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public SecondReport()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            cmbReason.DropDownStyle = ComboBoxStyle.DropDownList;
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT App.reason, App.date_start, App.date_end, App.country, App.city, App.worker_id, App.manager_confirmation FROM [dbo].[Application] App INNER JOIN [dbo].[Advanced_Report] AR ON App.id = AR.application_id INNER JOIN [dbo].[Category] Ctg ON Ctg.id = AR.category_id WHERE App.city = 'London' and AR.currency = 'British Pound' and App.accountant_confirmation = 1", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvReport.DataSource = dataSet.Tables["SR"];
            dgvReport.Refresh();
            con.Close();
            dgvReport.AllowUserToAddRows = false;
            dgvReport.AllowUserToDeleteRows = false;

            con.Open();
            SqlCommand selectReason = new SqlCommand("SELECT App.reason FROM [dbo].[Application] App INNER JOIN [dbo].[Advanced_Report] AR ON App.id = AR.application_id INNER JOIN [dbo].[Category] Ctg ON Ctg.id = AR.category_id WHERE App.city = 'London' and AR.currency = 'British Pound' and App.accountant_confirmation = 1", con);
            SqlDataReader DR = selectReason.ExecuteReader();

            cmbReason.Items.Clear();
            while (DR.Read())
            {
                cmbReason.Items.Add(DR[0]);
            }
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter($"SELECT App.reason, App.date_start, App.date_end, App.country, App.city, App.worker_id, App.manager_confirmation FROM [dbo].[Application] App INNER JOIN [dbo].[Advanced_Report] AR ON App.id = AR.application_id INNER JOIN [dbo].[Category] Ctg ON Ctg.id = AR.category_id WHERE App.city = 'London' and AR.currency = 'British Pound' and App.accountant_confirmation = 1 and App.reason = '{cmbReason.Text.ToString()}'", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvReport.DataSource = dataSet.Tables["SR"];
            dgvReport.Refresh();
            con.Close();
            dgvReport.AllowUserToAddRows = false;
            dgvReport.AllowUserToDeleteRows = false;
        }
    }
}
