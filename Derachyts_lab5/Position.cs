﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Derachyts_lab5
{
    class Position
    {
        public int id { get; set; }
        public string name { get; set; }

        public override string ToString()
        {
            return $"{name}";
        }
    }
}
