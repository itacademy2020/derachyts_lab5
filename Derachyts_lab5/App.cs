﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Derachyts_lab5
{
    class App
    {
        public int id { get; set; }
        public string reason { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
        public string manager_confirmation { get; set; }
        public string accountant_confirmation { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public int worker_id { get; set; }

        public override string ToString()
        {
            return $"{reason}; {date_start}; {date_end}";
        }

    }
}
