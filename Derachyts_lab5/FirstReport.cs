﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class FirstReport : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public FirstReport()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            cmbWorker.DropDownStyle = ComboBoxStyle.DropDownList;
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT App.date_start, App.date_end, App.country, App.city, Worker.full_name as 'Worker name',AR.currency,AR.amount, Ctg.name as 'Category name' FROM [dbo].[Application] App FULL OUTER JOIN [dbo].[Advanced_Report] AR ON App.id = AR.application_id inner join Worker ON Worker.id = App.worker_id full outer join [dbo].[Category] Ctg ON Ctg.id = AR.category_id", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvReport.DataSource = dataSet.Tables["SR"];
            dgvReport.Refresh();
            con.Close();
            dgvReport.AllowUserToAddRows = false;
            dgvReport.AllowUserToDeleteRows = false;

            con.Open();
            SqlCommand selectWorker = new SqlCommand("SELECT id, [full_name] FROM [dbo].[Worker]", con);
            SqlDataReader DR = selectWorker.ExecuteReader();

            cmbWorker.Items.Clear();
            while (DR.Read())
            {
                Worker worker = new Worker();
                worker.id = (int)DR[0];
                worker.full_name = (string)DR[1];
                cmbWorker.Items.Add(worker);
            }

            
            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter($"SELECT App.date_start, App.date_end, App.country, App.city, Worker.full_name as 'Worker name',AR.currency,AR.amount, Ctg.name as 'Category name' FROM [dbo].[Application] App FULL OUTER JOIN [dbo].[Advanced_Report] AR ON App.id = AR.application_id inner join Worker ON Worker.id = App.worker_id full outer join [dbo].[Category] Ctg ON Ctg.id = AR.category_id WHERE Worker.full_name = '{cmbWorker.Text.ToString()}'", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvReport.DataSource = dataSet.Tables["SR"];
            dgvReport.Refresh();
            con.Close();
            dgvReport.AllowUserToAddRows = false;
            dgvReport.AllowUserToDeleteRows = false;
        }
    }
}
