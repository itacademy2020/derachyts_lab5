﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class WorkerForm : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public WorkerForm()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            cmbManager.DropDownStyle = ComboBoxStyle.DropDownList;
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlCommand selectManager = new SqlCommand("SELECT id, [full_name] FROM [dbo].[Crew_Member]", con);
            SqlDataReader DR = selectManager.ExecuteReader();
            
            cmbManager.Items.Clear();
            while (DR.Read())
            {
                CrewMember crewMember = new CrewMember();
                crewMember.id = (int)DR[0];
                crewMember.full_name = (string)DR[1];
                cmbManager.Items.Add(crewMember);
            }

            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            CrewMember crewMember = (CrewMember)cmbManager.SelectedItem;

            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            dataValidation.Add("full_name", workerFullNameBox.Text.ToString());
            dataValidation.Add("contacts", contactsBox.Text.ToString());
            dataValidation.Add("manager", cmbManager.Text.ToString());
            String validationString = this.validateData(dataValidation);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
                return;
            }

            SqlCommand insertCrewMember = new SqlCommand($"INSERT INTO [dbo].[Worker] (full_name, contacts, crew_member_id) VAlUES ('{workerFullNameBox.Text.ToString()}', '{contactsBox.Text.ToString()}', {crewMember.id})", con);
            insertCrewMember.ExecuteNonQuery();
            con.Close();
            button2.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Worker]", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvWorkersRes.DataSource = dataSet.Tables["SR"];
            dgvWorkersRes.AllowUserToAddRows = false;
            dgvWorkersRes.AllowUserToDeleteRows = false;
            dgvWorkersRes.Columns["id"].ReadOnly = true;
            dgvWorkersRes.Columns["crew_member_id"].ReadOnly = true;
            dgvWorkersRes.Refresh();
            con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(sqlconnectionstring))
            {
                string sql = "SELECT * FROM [dbo].[Worker]";
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, "Table");

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);

                int i = 0;
                foreach (DataGridViewRow row in dgvWorkersRes.Rows)
                {
                    Dictionary<string, string> dataValidation = new Dictionary<string, string>();
                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataValidation.Add(column.ColumnName, row.Cells[column.ColumnName].Value.ToString());
                    }
                    String validationString = this.validateData(dataValidation);
                    if (!String.IsNullOrEmpty(validationString))
                    {
                        MessageBox.Show(validationString);
                        return;
                    }

                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataSet.Tables["Table"].Rows[i].SetField(column, row.Cells[column.ColumnName].Value);
                    }
                    i++;
                }

                adapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                adapter.Update(dataSet);
                dgvWorkersRes.DataSource = dataSet.Tables["Table"];
                dgvWorkersRes.Refresh();
                connection.Close();
            }
        }
        private string validateData(Dictionary<string, string> data)
        {
            foreach (var item in data)
            {
                switch (item.Key)
                {
                    case "full_name":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Full name is empty";
                        }
                        else if (item.Value.Length > 500)
                        {
                            return "Full name can't be longer than 500 characters";
                        }
                        break;
                    case "contacts":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field contacts is empty";
                        }
                        else if (item.Value.Length > 500)
                        {
                            return "Contacts can't be longer than 100 characters";
                        }
                        break;
                    case "manager":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field manager is empty";
                        }
                        break;
                }
            }
            return null;
        }
        private void tableDataValidation(object sender, DataGridViewDataErrorEventArgs e)
        {
            //String validationString = this.validateRow(sender,e.RowIndex,e.ColumnIndex);
            //if (!String.IsNullOrEmpty(validationString))
            //{
            //    //MessageBox.Show(validationString);
            //}
            e.ThrowException = false;
        }

        private void cellChanged(object sender, DataGridViewCellValidatingEventArgs e)
        {
            String validationString = this.validateRow(sender, e.RowIndex, e.ColumnIndex);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
            }
        }
        private string validateRow(object sender, int rowIndex, int columnIndex)
        {
            DataGridView dataGrid = (DataGridView)sender;
            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            foreach (DataGridViewColumn column in dataGrid.Columns)
            {
                if (column.Name == dataGrid.Columns[columnIndex].Name)
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].EditedFormattedValue.ToString());
                }
                else
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].Value.ToString());
                }

            }
            return this.validateData(dataValidation);
        }
    }
}
