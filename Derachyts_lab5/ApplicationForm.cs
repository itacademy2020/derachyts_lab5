﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class ApplicationForm : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public ApplicationForm()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            cmbWorker.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbManagerConf.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbAccountantConf.DropDownStyle = ComboBoxStyle.DropDownList;
            
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlCommand selectWorker = new SqlCommand("SELECT id, [full_name] FROM [dbo].[Worker]", con);
            SqlDataReader DR = selectWorker.ExecuteReader();

            cmbWorker.Items.Clear();
            while (DR.Read())
            {
                Worker worker = new Worker();
                worker.id = (int)DR[0];
                worker.full_name = (string)DR[1];
                cmbWorker.Items.Add(worker);
            }

            con.Close();

            cmbManagerConf.Items.Clear();
            cmbManagerConf.Items.AddRange(new object[] { "1", "0", "NULL" });
            cmbAccountantConf.Items.Clear();
            cmbAccountantConf.Items.AddRange(new object[] { "1", "0", "NULL" });
            cmbManagerConf.SelectedIndex = 2;
            cmbAccountantConf.SelectedIndex = 2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            Worker worker = (Worker)cmbWorker.SelectedItem;

            string managerConf = cmbManagerConf.Text.ToString();
            string accountantConf = cmbAccountantConf.Text.ToString();

            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            dataValidation.Add("reason", reasonBox.Text.ToString());
            dataValidation.Add("date start", dateStartBox.Text.ToString());
            dataValidation.Add("date end", dateEndBox.Text.ToString());
            dataValidation.Add("country", countryBox.Text.ToString());
            dataValidation.Add("city", cityBox.Text.ToString());
            dataValidation.Add("worker", cmbWorker.Text.ToString());
            String validationString = this.validateData(dataValidation);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
                return;
            }

            DateTime dateStart = DateTime.Parse(dateStartBox.Text.ToString());
            DateTime dateEnd = DateTime.Parse(dateStartBox.Text.ToString());


            SqlCommand insertCrewMember = new SqlCommand($"INSERT INTO [dbo].[Application] (reason, date_start, date_end, manager_confirmation, accountant_confirmation, country, city, worker_id) VAlUES ('{reasonBox.Text.ToString()}', '{dateStart.ToString("yyyy-MM-dd")}', '{dateEnd.ToString("yyyy-MM-dd")}', {managerConf}, {accountantConf}, '{countryBox.Text.ToString()}', '{cityBox.Text.ToString()}', {worker.id})", con);
            insertCrewMember.ExecuteNonQuery();
            con.Close();
            button2.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Application]", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvAppRes.DataSource = dataSet.Tables["SR"];
            dgvAppRes.AllowUserToAddRows = false;
            dgvAppRes.AllowUserToDeleteRows = false;
            dgvAppRes.Columns["id"].ReadOnly = true;
            dgvAppRes.Columns["worker_id"].ReadOnly = true;
            dgvAppRes.Refresh();
            con.Close();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(sqlconnectionstring))
            {
                string sql = "SELECT * FROM dbo.Application";
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, "Table");

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);

                int i = 0;
                foreach (DataGridViewRow row in dgvAppRes.Rows)
                {
                    Dictionary<string, string> dataValidation = new Dictionary<string, string>();
                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataValidation.Add(column.ColumnName, row.Cells[column.ColumnName].Value.ToString());
                    }
                    String validationString = this.validateData(dataValidation);
                    if (!String.IsNullOrEmpty(validationString))
                    {
                        MessageBox.Show(validationString);
                        return;
                    }
                    
                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        if(column.ColumnName == "date_start" || column.ColumnName == "date_end")
                        {
                            DateTime dateTime = DateTime.Parse(row.Cells[column.ColumnName].Value.ToString());
                            
                            dataSet.Tables["Table"].Rows[i].SetField(column, dateTime.ToString("yyyy-MM-dd"));
                        }
                        dataSet.Tables["Table"].Rows[i].SetField(column, row.Cells[column.ColumnName].Value);
                    }
                    i++;
                }

                adapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                adapter.Update(dataSet);
                dgvAppRes.DataSource = dataSet.Tables["Table"];
                dgvAppRes.Refresh();
                connection.Close();
            }
        }
        private string validateData(Dictionary<string, string> data)
        {
            foreach (var item in data)
            {
                switch (item.Key)
                {
                    case "reason":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field reason is empty";
                        }
                        else if (item.Value.Length > 500)
                        {
                            return "Reason can't be longer than 500 characters";
                        }
                        break;
                    case "date start":
                        if (!DateTime.TryParse(item.Value, out DateTime result))
                        {
                            return "Field date start is not correct";
                        }
                        else if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field date start is empty";
                        }
                        break;
                    case "date end":
                        if (!DateTime.TryParse(item.Value, out DateTime res))
                        {
                            return "Field date end is not correct";
                        }
                        else if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field date end is empty";
                        }
                        break;
                    case "country":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field country is empty";
                        }
                        else if (item.Value.Length > 60)
                        {
                            return "Country can't be longer than 60 characters";
                        }
                        break;
                    case "city":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field city is empty";
                        }
                        else if (item.Value.Length > 200)
                        {
                            return "City can't be longer than 200 characters";
                        }
                        break;
                    case "worker":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field worker is empty";
                        }
                        break;
                }
            }
            return null;
        }
        private void tableDataValidation(object sender, DataGridViewDataErrorEventArgs e)
        {
            //String validationString = this.validateRow(sender,e.RowIndex,e.ColumnIndex);
            //if (!String.IsNullOrEmpty(validationString))
            //{
            //    //MessageBox.Show(validationString);
            //}
            e.ThrowException = false;
        }

        private void cellChanged(object sender, DataGridViewCellValidatingEventArgs e)
        {
            String validationString = this.validateRow(sender, e.RowIndex, e.ColumnIndex);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
            }
        }
        private string validateRow(object sender, int rowIndex, int columnIndex)
        {
            DataGridView dataGrid = (DataGridView)sender;
            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            foreach (DataGridViewColumn column in dataGrid.Columns)
            {
                if (column.Name == dataGrid.Columns[columnIndex].Name)
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].EditedFormattedValue.ToString());
                }
                else
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].Value.ToString());
                }

            }
            return this.validateData(dataValidation);
        }
    }
}
