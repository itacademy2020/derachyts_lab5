﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class AdvancedReportForm : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public AdvancedReportForm()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            cmbWorker.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbApplication.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbCategory.DropDownStyle = ComboBoxStyle.DropDownList;
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlCommand selectWorker = new SqlCommand("SELECT DISTINCT Wrk.id, Wrk.full_name FROM [dbo].[Application] App inner join [dbo].[Worker] Wrk ON App.worker_id = Wrk.id", con);
            SqlDataReader DRselWrk = selectWorker.ExecuteReader();

            cmbWorker.Items.Clear();
            while (DRselWrk.Read())
            {
                Worker worker = new Worker();
                worker.id = (int)DRselWrk[0];
                worker.full_name = (string)DRselWrk[1];
                cmbWorker.Items.Add(worker);
            }
            DRselWrk.Close();

            SqlCommand selectCategory = new SqlCommand("SELECT id, name, [description] FROM [dbo].[Category]", con);
            SqlDataReader DRselCat = selectCategory.ExecuteReader();
            
            cmbCategory.Items.Clear();
            while (DRselCat.Read())
            {
                Category category = new Category();
                category.id = (int)DRselCat[0];
                category.name = (string)DRselCat[1];
                category.description = (string)DRselCat[2];
                cmbCategory.Items.Add(category);
            }

            con.Close();
        }

        private void selectWorkerHandler(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            Worker currentWrk = (Worker)cmbWorker.SelectedItem;

            SqlCommand selectApplication = new SqlCommand($"SELECT id, reason, date_start, date_end FROM [dbo].[Application] WHERE worker_id = '{currentWrk.id}'", con);
            SqlDataReader DRselApp = selectApplication.ExecuteReader();

            cmbApplication.Items.Clear();
            while (DRselApp.Read())
            {
                App app = new App();
                app.id = (int)DRselApp[0];
                app.reason = (string)DRselApp[1];
                app.date_start = DRselApp[2].ToString();
                app.date_end = DRselApp[3].ToString();
                cmbApplication.Items.Add(app);
            }
            DRselApp.Close();

            con.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            App app = (App)cmbApplication.SelectedItem;
            Category category = (Category)cmbCategory.SelectedItem;
            string amount = amountBox.Text.ToString();
            string currency = currencyBox.Text.ToString();

            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            dataValidation.Add("application", cmbApplication.Text.ToString());
            dataValidation.Add("currency", currencyBox.Text.ToString());
            dataValidation.Add("amount", amountBox.Text.ToString());
            dataValidation.Add("category", cmbCategory.Text.ToString());
            String validationString = this.validateData(dataValidation);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
                return;
            }

            SqlCommand insertCrewMember = new SqlCommand($"INSERT INTO [dbo].[Advanced_report] (currency, amount, application_id, category_id) VAlUES ('{currency}', '{amount}', '{app.id}', '{category.id}')", con);
            insertCrewMember.ExecuteNonQuery();
            
            con.Close();
            button2.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Advanced_Report]", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvAdvRepRes.DataSource = dataSet.Tables["SR"];
            dgvAdvRepRes.AllowUserToAddRows = false;
            dgvAdvRepRes.AllowUserToDeleteRows = false;
            dgvAdvRepRes.Columns["id"].ReadOnly = true;
            dgvAdvRepRes.Columns["application_id"].ReadOnly = true;
            dgvAdvRepRes.Columns["category_id"].ReadOnly = true;
            dgvAdvRepRes.Refresh();

            con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(sqlconnectionstring))
            {
                string sql = "SELECT * FROM [dbo].[Advanced_Report]";
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, "Table");

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);

                int i = 0;
                foreach (DataGridViewRow row in dgvAdvRepRes.Rows)
                {
                    Dictionary<string, string> dataValidation = new Dictionary<string, string>();
                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataValidation.Add(column.ColumnName, row.Cells[column.ColumnName].Value.ToString());
                    }
                    String validationString = this.validateData(dataValidation);
                    if (!String.IsNullOrEmpty(validationString))
                    {
                        MessageBox.Show(validationString);
                        return;
                    }
                    
                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataSet.Tables["Table"].Rows[i].SetField(column, row.Cells[column.ColumnName].Value);
                    }
                    i++;
                }

                adapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                adapter.Update(dataSet);
                dgvAdvRepRes.DataSource = dataSet.Tables["Table"];
                dgvAdvRepRes.Refresh();
                connection.Close();
            }
        }
        private string validateData(Dictionary<string, string> data)
        {
            foreach (var item in data)
            {
                switch (item.Key)
                {
                    case "application":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Application is empty";
                        }
                        break;
                    case "currency":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Currency is empty";
                        }
                        else if (item.Value.Length > 50)
                        {
                            return "Currency can't be longer than 50 characters";
                        }
                        break;
                    case "amount":
                        if (!int.TryParse(item.Value, out int result))
                        {
                            return "Field amount is not correct";
                        }
                        break;
                    case "category":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field category is empty";
                        }
                        break;
                }
            }
            return null;
        }
        private void tableDataValidation(object sender, DataGridViewDataErrorEventArgs e)
        {
            //String validationString = this.validateRow(sender,e.RowIndex,e.ColumnIndex);
            //if (!String.IsNullOrEmpty(validationString))
            //{
            //    //MessageBox.Show(validationString);
            //}
            e.ThrowException = false;
        }

        private void cellChanged(object sender, DataGridViewCellValidatingEventArgs e)
        {
            String validationString = this.validateRow(sender, e.RowIndex, e.ColumnIndex);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
            }
        }
        private string validateRow(object sender, int rowIndex, int columnIndex)
        {
            DataGridView dataGrid = (DataGridView)sender;
            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            foreach (DataGridViewColumn column in dataGrid.Columns)
            {
                if (column.Name == dataGrid.Columns[columnIndex].Name)
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].EditedFormattedValue.ToString());
                }
                else
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].Value.ToString());
                }

            }
            return this.validateData(dataValidation);
        }
    }
}
