﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Derachyts_lab5
{
    class Worker
    {
        public int id { get; set; }
        public string full_name { get; set; }
        public string contacts { get; set; }
        public int crew_member_id { get; set; }

        public override string ToString()
        {
            return $"{full_name}"; 
        }
    }
}
