﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Derachyts_lab5
{
    class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

        public override string ToString()
        {
            return $"{description}";
        }
    }
}
