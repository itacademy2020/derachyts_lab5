﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Derachyts_lab5
{
    class AdvancedReport
    {
        public int id { get; set; }
        public string currency { get; set; }
        public int amount { get; set; }
        public int application_id { get; set; }
        public int category_id { get; set; }

        public override string ToString()
        {
            return $"{currency}; {amount}";
        }
    }
}
