﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class CrewMemberForm : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public CrewMemberForm()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            cmbPosition.DropDownStyle = ComboBoxStyle.DropDownList;
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlCommand selectPosition = new SqlCommand("SELECT id, [name] FROM [dbo].[Position]", con);
            SqlDataReader DR = selectPosition.ExecuteReader();

            cmbPosition.Items.Clear();
            while (DR.Read())
            {
                Position position = new Position();
                position.id = (int)DR[0];
                position.name = (string)DR[1];
                cmbPosition.Items.Add(position);
            }

            con.Close();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            Position position = (Position)cmbPosition.SelectedItem;

            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            dataValidation.Add("full_name", FullNameBox.Text.ToString());
            dataValidation.Add("age", AgeBox.Text.ToString());
            dataValidation.Add("adress", AdressBox.Text.ToString());
            dataValidation.Add("position", cmbPosition.Text.ToString());
            String validationString = this.validateData(dataValidation);
            if (!String.IsNullOrEmpty(validationString)) 
            {
                MessageBox.Show(validationString);
                return;
            }

            SqlCommand insertCrewMember = new SqlCommand($"INSERT INTO [dbo].[Crew_Member] (full_name, age, adress, position_id) VAlUES ('{FullNameBox.Text.ToString()}', {AgeBox.Text.ToString()} , '{AdressBox.Text.ToString()}', {position.id})", con);
            insertCrewMember.ExecuteNonQuery();
            con.Close();
            button2.PerformClick();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Crew_Member]", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvResults.DataSource = dataSet.Tables["SR"];
            dgvResults.AllowUserToAddRows = false;
            dgvResults.AllowUserToDeleteRows = false;
            dgvResults.Columns["id"].ReadOnly = true;
            dgvResults.Columns["position_id"].ReadOnly = true;
            dgvResults.Refresh();
            con.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(sqlconnectionstring))
            {
                string sql = "SELECT * FROM [dbo].[Crew_Member]";
                connection.Open();

                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet,"Table");

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(adapter);

                int i = 0;
                foreach(DataGridViewRow row in dgvResults.Rows)
                {
                    Dictionary<string, string> dataValidation = new Dictionary<string, string>();
                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataValidation.Add(column.ColumnName, row.Cells[column.ColumnName].Value.ToString());
                    }
                    String validationString = this.validateData(dataValidation);
                    if (!String.IsNullOrEmpty(validationString))
                    {
                        MessageBox.Show(validationString);
                        return;
                    }

                    foreach (DataColumn column in dataSet.Tables["Table"].Columns)
                    {
                        dataSet.Tables["Table"].Rows[i].SetField(column, row.Cells[column.ColumnName].Value);
                    }
                    i++;
                }
                
                adapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                adapter.Update(dataSet);
                dgvResults.DataSource = dataSet.Tables["Table"];
                dgvResults.Refresh();
                connection.Close();
            }
        }
        private string validateData(Dictionary<string, string> data)
        {
            foreach (var item in data)
            {
                switch (item.Key)
                {
                    case "full_name":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Full name is empty";
                        }
                        else if (item.Value.Length > 500)
                        {
                            return "Full name can't be longer than 500 characters";
                        }
                        break;
                    case "age":
                        if (!int.TryParse(item.Value, out int result))
                        {
                            return "Field age is not correct";
                        }
                        else if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field age is empty";
                        }
                        break;
                    case "adress":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field address is empty";
                        }
                        break;
                    case "position":
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            return "Field position is empty";
                        }
                        break;
                }
            }
            return null;
        }
        
        private void tableDataValidation(object sender, DataGridViewDataErrorEventArgs e)
        {
            //String validationString = this.validateRow(sender,e.RowIndex,e.ColumnIndex);
            //if (!String.IsNullOrEmpty(validationString))
            //{
            //    //MessageBox.Show(validationString);
            //}
            e.ThrowException = false;
        }

        private void cellChanged(object sender, DataGridViewCellValidatingEventArgs e)
        {
            String validationString = this.validateRow(sender, e.RowIndex,e.ColumnIndex);
            if (!String.IsNullOrEmpty(validationString))
            {
                MessageBox.Show(validationString);
            }
        }
        private string validateRow(object sender,int rowIndex,int columnIndex)
        {
            DataGridView dataGrid = (DataGridView)sender;
            Dictionary<string, string> dataValidation = new Dictionary<string, string>();
            foreach (DataGridViewColumn column in dataGrid.Columns)
            {
                if (column.Name == dataGrid.Columns[columnIndex].Name)
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].EditedFormattedValue.ToString());
                }
                else
                {
                    dataValidation.Add(column.Name, dataGrid.Rows[rowIndex].Cells[column.Name].Value.ToString());
                }
                
            }
            return this.validateData(dataValidation);
        }
    }
}
