﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Derachyts_lab5
{
    public partial class ThirdReport : Form
    {
        string sqlconnectionstring = "Server=tcp:itacademy.database.windows.net,1433;Database=Derachyts;User Id=Derachyts;Password=Pd6VF1sf;Trusted_Connection=False;Encrypt=True;";
        public ThirdReport()
        {
            InitializeComponent();
        }

        private void Form8_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int managerConf = 0;
            if (checkBox.Checked)
            {
                managerConf = 1;
            }
            SqlConnection con = new SqlConnection(sqlconnectionstring);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter($"SELECT Worker.id, Worker.full_name as 'Worker name', Worker.contacts, Worker.crew_member_id FROM [dbo].[Worker] WHERE Worker.id NOT IN (SELECT Wrk.id FROM [dbo].[Application] App INNER JOIN [dbo].[Worker] Wrk ON Wrk.id = App.worker_id WHERE App.manager_confirmation = {managerConf})", con);

            DataSet dataSet = new DataSet();
            DA.Fill(dataSet, "SR");
            dgvReport.DataSource = dataSet.Tables["SR"];
            dgvReport.Refresh();
            con.Close();
            dgvReport.AllowUserToAddRows = false;
            dgvReport.AllowUserToDeleteRows = false;
        }
    }
}
